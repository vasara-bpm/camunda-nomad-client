"""Health state."""
from camunda_nomad_client.health.types import State


state = State()
