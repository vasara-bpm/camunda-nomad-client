"""Health tasks."""
from camunda_nomad_client.camunda.types import CompleteExternalTaskDto
from camunda_nomad_client.camunda.types import LockedExternalTaskDto
from camunda_nomad_client.camunda.types import VariableValueDto
from camunda_nomad_client.health.config import Topic
from camunda_nomad_client.health.state import state
from camunda_nomad_client.types import ExternalTaskComplete
from datetime import datetime


async def heartbeat(task: LockedExternalTaskDto) -> ExternalTaskComplete:
    """Update health check timestamp."""
    state.heartbeat = datetime.utcnow().isoformat()
    return ExternalTaskComplete(
        task=task,
        response=CompleteExternalTaskDto(
            workerId=task.workerId,
            variables={
                "heartbeat": VariableValueDto(value=state.heartbeat, type="string"),
            },
        ),
    )


TASKS = {Topic.HEALTH_HEARTBEAT: heartbeat}
