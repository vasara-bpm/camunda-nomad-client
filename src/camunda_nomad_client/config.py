"""Settings and logging configuration."""
# https://pydantic-docs.helpmanual.io/usage/settings/
from camunda_nomad_client.health.config import Settings as health_settings
from camunda_nomad_client.nomad.config import Settings as nomad_settings
from pygelf import GelfUdpHandler
from typing import Optional
import logging


class Settings(health_settings, nomad_settings):
    """Settings."""

    APP_NAME: str = __name__.split(".", 1)[0]

    CAMUNDA_API_PATH: str = "http://localhost:8080/engine-rest"
    CAMUNDA_API_AUTHORIZATION: str = ""
    CAMUNDA_TIMEOUT: int = 20
    CAMUNDA_POLL_TTL: int = 10
    CAMUNDA_LOCK_TTL: int = 60

    AIOHTTP_TIMEOUT: int = 10

    GELF_HOST: Optional[str] = None
    GELF_PORT: Optional[int] = None

    LOG_LEVEL: str = "DEBUG"


settings = Settings()


formatter = logging.Formatter(
    "%(asctime)s | %(levelname)s | %(name)s:%(lineno)d | %(message)s",
    "%d-%m-%Y %H:%M:%S",
)

stream_handler = logging.StreamHandler()
stream_handler.setFormatter(formatter)
stream_handler.setLevel(settings.LOG_LEVEL)

logger = logging.getLogger(__name__.split(".", 1)[0])
logger.addHandler(stream_handler)
logger.setLevel(settings.LOG_LEVEL)
logger.propagate = False

if settings.GELF_HOST and settings.GELF_PORT:
    logger.addHandler(
        GelfUdpHandler(
            host=settings.GELF_HOST,
            port=settings.GELF_PORT,
            _app_name=settings.APP_NAME,
        )
    )

__all__ = ["settings"]
