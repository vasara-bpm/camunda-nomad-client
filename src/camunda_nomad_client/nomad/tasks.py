"""Nomad tasks."""
from aiohttp import ClientTimeout
from base64 import b64decode
from base64 import b64encode
from camunda_nomad_client.camunda.types import ExternalTaskFailureDto
from camunda_nomad_client.camunda.types import LockedExternalTaskDto
from camunda_nomad_client.config import settings
from camunda_nomad_client.nomad.config import Topic
from camunda_nomad_client.nomad.types import CONFIGURATION_VARIABLE_NAME
from camunda_nomad_client.nomad.types import JobAllocation
from camunda_nomad_client.nomad.types import JobDispatchRequest
from camunda_nomad_client.nomad.types import JobDispatchResponse
from camunda_nomad_client.nomad.types import RobotJobConfiguration
from camunda_nomad_client.nomad.types import TaskLog
from camunda_nomad_client.types import ExternalTaskComplete
from camunda_nomad_client.types import ExternalTaskFailure
from camunda_nomad_client.types import NoOp
from camunda_nomad_client.utils import as_json
from camunda_nomad_client.utils import assert_status_code
from camunda_nomad_client.variables import task_variable_map
from contextlib import asynccontextmanager
from pydantic import ValidationError
from typing import AsyncGenerator
from typing import Union
import aiohttp
import asyncio
import logging
import yaml


logger = logging.getLogger(__name__)


@asynccontextmanager
async def nomad_session(
    authorization: str = settings.NOMAD_AUTHORIZATION,
) -> AsyncGenerator[aiohttp.ClientSession, None]:
    """Get aiohttp session with headers for Nomad HTTP API."""
    headers = (
        {
            "Content-Type": "application/json",
            "Accept": "application/json",
            "Authorization": authorization,
        }
        if authorization
        else {"Content-Type": "application/json", "Accept": "application/json"}
    )
    async with aiohttp.ClientSession(
        headers=headers,
        trust_env=True,
        timeout=ClientTimeout(total=settings.NOMAD_TIMEOUT),
    ) as session:
        yield session


def b64dump(s: str) -> str:
    """Convert string into base64 string."""
    return b64encode(s.encode("utf-8")).decode("utf-8")


async def execute_nomad_robot(
    task: LockedExternalTaskDto,
) -> Union[ExternalTaskComplete, ExternalTaskFailure]:
    """Execute parameterized Nomad job for Robot Framework bot."""
    logger.info(task.json())
    try:
        config = RobotJobConfiguration(
            **await task_variable_map(task, CONFIGURATION_VARIABLE_NAME)
        )
    except (KeyError, ValidationError) as e:
        return ExternalTaskFailure(
            task=task,
            response=ExternalTaskFailureDto(
                workerId=task.workerId,
                errorMessage="Invalid Nomad task configuration.",
                errorDetails=f"{e}",
                retries=0,
            ),
        )

    variables = {
        "CAMUNDA_TASK_ID": task.id,
        "CAMUNDA_TASK_RETRIES": max(task.retries - 1, 0) if task.retries else 0,
        "CAMUNDA_TASK_WORKER_ID": task.workerId,
        "CAMUNDA_TASK_PROCESS_INSTANCE_ID": task.processInstanceId,
        "CAMUNDA_TASK_EXECUTION_ID": task.executionId,
    }

    url = f"{settings.NOMAD_API_PATH}/v1/job/{config.job}/dispatch"
    request = JobDispatchRequest(
        Payload=b64dump(yaml.dump(variables)),
        Meta={
            "suite_name": config.suite,
            "task_name": config.task or "*",
            "log_level": settings.LOG_LEVEL,
        },
    )
    async with nomad_session() as http:
        post = await http.post(url, data=as_json(request))
        await assert_status_code(post, code=(200,))
        response = JobDispatchResponse(**await post.json())

    url = f"{settings.NOMAD_API_PATH}/v1/job/{response.DispatchedJobID}/allocations"
    while True:
        async with nomad_session() as http:
            get = await http.get(url, data=as_json(request))
            await assert_status_code(post, code=(200,))
            response_ = [JobAllocation(**item) for item in await get.json()]

        logger.debug("Nomad job '%s' status '%s'", task.id, response_)

        for allocation in response_:
            if allocation.ClientStatus in ["complete"]:
                return ExternalTaskComplete(task=task, response=NoOp())
            if allocation.ClientStatus in ["failed"] and allocation.TaskStates:
                messages = []
                for name in allocation.TaskStates.keys():
                    url = f"{settings.NOMAD_API_PATH}/v1/client/fs/logs/{allocation.ID}"
                    params = {"task": name, "type": "stderr"}
                    async with nomad_session() as http:
                        get = await http.get(url, params=params)
                        await assert_status_code(post, code=(200,))
                        log = TaskLog(**await get.json())
                        messages.append(
                            b64decode(log.Data.encode("utf-8")).decode("utf-8")
                        )
                return ExternalTaskFailure(
                    task=task,
                    response=ExternalTaskFailureDto(
                        workerId=task.workerId,
                        errorMessage=allocation.ClientStatus,
                        errorDetails="\n".join(messages),
                        retries=0,
                    ),
                )

        # We keep this task alive to allow worker to keep renewing the lock
        await asyncio.sleep(settings.CAMUNDA_POLL_TTL)
        # Pros: When nomad is busy, this worker can keep renewing the lock
        # Cons: If worker is restarted the task may be rescheduled by Camunda


TASKS = {Topic.NOMAD_ROBOT_TASK: execute_nomad_robot}
