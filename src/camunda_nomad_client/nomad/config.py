"""Nomad configuration."""
from enum import Enum
from pydantic import BaseSettings


class Settings(BaseSettings):
    """Settings."""

    NOMAD_API_PATH: str = "http://localhost:4646"
    NOMAD_AUTHORIZATION: str = ""
    NOMAD_TIMEOUT: int = 60


class Topic(str, Enum):
    """External task topics."""

    NOMAD_ROBOT_TASK = "nomad.robot"
