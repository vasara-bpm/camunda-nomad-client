"""Nomad types."""
from pydantic import BaseModel
from typing import Dict
from typing import List
from typing import Optional


CONFIGURATION_VARIABLE_NAME = "configuration"


class RobotJobConfiguration(BaseModel):
    """Robot Framework job configuration."""

    job: str
    suite: str
    task: Optional[str] = None


class JobDispatchRequest(BaseModel):
    """Nomad job-dispatch request."""

    Payload: str
    Meta: Dict[str, str]


class JobDispatchResponse(BaseModel):
    """Nomad job-dispatch response."""

    Index: int
    JobCreateIndex: int
    EvalCreateIndex: int
    EvalID: str
    DispatchedJobID: str


class TaskLog(BaseModel):
    """Log"""

    Data: str


class TaskState(BaseModel):
    """Nomad job allocation task state"""

    State: str


class JobAllocation(BaseModel):
    """Nomad job allocation"""

    ID: str
    ClientStatus: str
    TaskStates: Optional[Dict[str, TaskState]]


class JobAllocationsResponse(BaseModel):
    """Nomad job allocations response."""

    __root__: List[JobAllocation]
