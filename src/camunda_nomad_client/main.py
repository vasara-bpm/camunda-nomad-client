"""Camunda Nomad Client."""
from camunda_nomad_client.health.routes import router as health_router
from camunda_nomad_client.health.tasks import TASKS as health_tasks
from camunda_nomad_client.nomad.tasks import TASKS as nomad_tasks
from camunda_nomad_client.types import ExternalTaskHandler
from camunda_nomad_client.types import TOPIC
from camunda_nomad_client.worker import external_task_worker
from fastapi.applications import FastAPI
from starlette.requests import Request
from starlette.responses import Response
from typing import Awaitable
from typing import Callable
from typing import Dict
import asyncio
import logging


logger = logging.getLogger(__name__)

app = FastAPI(
    title="Camunda Nomad Client API",
    description="Camunda external task client for dispatching Nomad jobs.",
    version="1.0.0",
)


@app.on_event("startup")
async def startup_event() -> None:
    """Start external task worker on FastAPI startup."""
    tasks: Dict[TOPIC, ExternalTaskHandler] = {}
    tasks.update(health_tasks)  # type: ignore
    tasks.update(nomad_tasks)  # type: ignore
    asyncio.ensure_future(external_task_worker(tasks))
    logger.info("Event loop: %s", asyncio.get_event_loop())


app.include_router(health_router)


@app.middleware("http")
async def cache_headers(
    request: Request, call_next: Callable[[Request], Awaitable[Response]]
) -> Response:
    """Set cache headers."""
    response = await call_next(request)
    response.headers["Cache-Control"] = "no-store, max-age=0"
    return response
