{ nixpkgs ? sources.nixpkgs
, config ? {}
, sources ? import ./sources.nix
}:

let

  overlay = _: pkgs: {

    gitignoreSource = (import sources.gitignore {
      inherit (pkgs) lib;
    }).gitignoreSource;

    # pip2nix branches require specific nixpkgs branch (for pip)
    pip2nix-20_09 = ((import (sources."pip2nix-20.09" + "/release.nix") {
      pkgs = import sources."nixpkgs-20.09" {};
    }).pip2nix);

  };

  pkgs = import nixpkgs {
    overlays = [ overlay ];
    inherit config;
  };

in pkgs
