Camunda Nomad External Task Client
==================================

Proof-of-concept of (eventually) generic Camunda external task client for dispatching parameterized Nomad scheduler jobs. And how to use it to orchestrate Robot Framework RPA bots.

Try out the demo
----------------

```bash
$ docker-compose up
```

Open http://localhost:8080/camunda with credentials `demo:demo` to login and start the example process *XKCD comic search*.

Nomad may be observed at http://localhost:4646

![](example.gif)


Hacking
-------

The development environment is based GNU make on Nix development tools, mostly following the documented tips at https://nix.dev

```bash
$ make shell
[nix-shell]$ make watch
```

Or

```bash
$ make nix-watch
```
