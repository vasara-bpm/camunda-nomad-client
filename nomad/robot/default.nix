{ pkgs ? import ../nix { nixpkgs = sources."nixpkgs-unstable"; }
, sources ? import ../nix/sources.nix
}:

let sitePackages = ./site-packages; in

pkgs.buildEnv {
  name = "robot";
  paths = with pkgs; [
    (import ./setup.nix { inherit pkgs; }).python
    firefox
    geckodriver
  ];
  buildInputs = with pkgs; [
    makeWrapper
  ];
  postBuild = ''
    wrapProgram $out/bin/robot \
      --prefix PATH : $out/bin \
      --prefix PYTHONPATH : ${sitePackages}
  '';
}
