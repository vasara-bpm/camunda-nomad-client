{ pkgs ? import ../nix { nixpkgs = sources."nixpkgs-unstable"; }
, sources ? import ../nix/sources.nix
}:

let sitePackages = ./site-packages; in

pkgs.mkShell {
  buildInputs = with pkgs; [
    # pip2nix-20_09.python39
    (import ./setup.nix { inherit pkgs; }).python
    firefox
    geckodriver
  ];
  shellHook = ''
    export PYTHONPATH=${sitePackages}:$PYTHONPATH
  '';
}
