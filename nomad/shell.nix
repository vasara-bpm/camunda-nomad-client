{ pkgs ? import ./nix { nixpkgs = sources."nixpkgs-unstable"; }
, sources ? import ./nix/sources.nix {}
}:

pkgs.mkShell {
  buildInputs = with pkgs; [
    nomad_1_0
  ];
}

