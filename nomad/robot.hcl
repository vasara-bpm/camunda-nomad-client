job "robot" {
  datacenters = ["dc1"]
  type = "batch"
  parameterized {
    payload = "required"
    meta_required = ["suite_name", "task_name", "log_level"]
  }
  group "task" {
    count = 1
    reschedule {
      attempts = 0
    }
    task "task" {
      driver = "raw_exec"
      restart {
        attempts = 0
        mode = "fail"
      }
      resources {
        cpu = 1024
        memory = 1024
      }
      env {
        ROBOT_LOG_LEVEL = "${NOMAD_META_LOG_LEVEL}"
      }
      config {
        command = "/bin/sh"
        args    = ["-c", <<EOH
exec ${NIX_SHELL} ${ROBOT_DIR}/shell.nix --run "
robot --rpa --loglevel ${ROBOT_LOG_LEVEL} --nostatusrc --exitonfailure -l NONE -r NONE --listener CamundaListener -V local/variables.yaml -t \"${NOMAD_META_TASK_NAME}\" ${ROBOT_DIR}/suites/${NOMAD_META_SUITE_NAME}.robot
"
EOH
          ]
      }
      dispatch_payload {
        file = "variables.yaml"
      }
    }

  }
}
