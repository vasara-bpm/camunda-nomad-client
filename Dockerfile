FROM nixos/nix:2.3.10
RUN nix-shell -p cachix --run "cachix use vasara-bpm"
RUN nix-build -E 'import (fetchTarball https://gitlab.com/vasara-bpm/camunda-nomad-client/-/archive/master/camunda-nomad-client-master.tar.gz + "/setup.nix")' -A appPython -o /app
ENTRYPOINT ["/app/bin/uvicorn", "camunda_nomad_client.main:app"]
