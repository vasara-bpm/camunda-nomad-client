# -*- coding: utf-8 -*-
"""Test that project is importable."""


def test_import() -> None:
    import camunda_nomad_client.main  # noqa
